# Coding Challenge

## Getting Started

This is small React Project, that has already some UI elements, but is missing most functionality.
The app should load fruit data from a JSON-File and display it in a list and on a detail page. 
Furthermore, the access should be limited to users that signed in before.

The project was set up with [yarn](https://yarnpkg.com/) (not npm). To start the project run `yarn install` and then `yarn start` in the project directory.

We use the following libraries:

- [Create React App](https://create-react-app.dev/) for setup with no configuration
- [Styled Components](https://www.styled-components.com/) for CSS styles
- [React Router](https://reactrouter.com/) for navigation
- [Lodash](https://lodash.com/) for utilities
- [Typescript](https://www.typescriptlang.org/) for type-safety

Typescript is configured to allow JavaScript in case you're not familiar with Typescript (yet), so you can mix both languages.

## Tasks

### 1. Full height page

Take a look onto the Login-Page [/login](http://localhost:3000/login). 
The Login-Box sticks to the top, what doesn't look nice.

- [ ] Write some CSS to center the Login-Box vertically and horizontally on the page.

> Tip: Go to `app/theme/GlobalStyles.tsx` to adjust  global css styles.

### 2. Load data from JSON-File and display it
Now move to the [Home-Page](http://localhost:3000). 
As you can see the page shows two products.
When you open `HomePage.tsx` you can see that they are added statically.

- [ ] Load data from a `fruits.json` (located in the `public` directory) using the [fetch()](https://developer.mozilla.org/de/docs/Web/API/Fetch_API) api
- [ ] Display all entries using the already existing `Product` component
- [ ] Show a `LoadingIndicator` (see `LoadingIndicator.tsx`) while loading the data
- [ ] Show an error message in case something fails
- [ ] Advanced: The `Product` component is currently rendered as a `div`.
      It would be nice if we could also render it as a `li` element. Add a new property `component` to `Product` that allows changing its root element.

### 3. Add a category filter
You might have noticed the filter tags at the top of the page. We want to use these to filter the fruits by category.

- [ ] Extract all existing categories from `fruits.json` and render one `FilterItem` for each category.
- [ ] Filter the fruit list by category when the user clicks one of the filters
- [ ] Show the currently active filter by adding a border around the selected `FilterItem`  

### 4. Login
Now it's time to protect our app from unauthorized access. Head over to `LoginPage.tsx`.

- [ ] Gather the form data when the user clicks the 'Login' button and call the mocked `login()` function provided by `login-api.ts`
- [ ] Make sure that `/` can only be accessed when the login was successful
- [ ] Store the authentication status persistently, so that refreshing the page doesn't log out the user. Why is this not a secure solution?

> `login()` expects the username to be _admin_ and the password to be _password_

