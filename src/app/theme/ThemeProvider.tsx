import React from "react";
import {ThemeProvider as TP} from "styled-components";
import {lightTheme} from "./theme";
import {GlobalStyle} from "./GlobalStyles";

const StyledThemeProvider: any = TP; // Styled-Components is not yet ready for React 18: https://github.com/styled-components/styled-components/issues/3738

interface Props {
    children: React.ReactNode;
}

export const ThemeProvider = ({children}: Props) => {
    return (
        <StyledThemeProvider theme={lightTheme}>
            <GlobalStyle/>
            {children}
        </StyledThemeProvider>
    );
};
