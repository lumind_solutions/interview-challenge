import { DefaultTheme } from "styled-components";

declare module "styled-components" {
  export interface DefaultTheme {
    colors: {
      primaryColor: string;
      lightGrey: string;
      darkGrey: string;
    };
    fontFamily: string;
  }
}

export const lightTheme: DefaultTheme = {
  colors: {
    primaryColor: "#3900e5",
    lightGrey: "#cdcdcd",
    darkGrey: "#393939",
  },
  fontFamily: "'Roboto Mono', monospace",
};
