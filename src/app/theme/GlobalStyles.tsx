import { createGlobalStyle } from "styled-components";
import "roboto-mono-webfont";

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    background-color: ${({ theme }) => theme.colors.lightGrey};
    font-family: 'Roboto Mono', monospace;
  }
` as React.ComponentType;
