import React from "react";
import { AppRouter } from "./routing";
import { ThemeProvider } from "./theme";
import { Layout } from "./layout";

function App() {
  return (
    <ThemeProvider>
      <Layout>
        <AppRouter />
      </Layout>
    </ThemeProvider>
  );
}

export default App;
