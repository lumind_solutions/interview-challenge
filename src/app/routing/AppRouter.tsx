import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { HOME, LOGIN } from "./routes";
import { LoginPage } from "../../components/login";
import { HomePage } from "../../components/home";

export const AppRouter = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={LOGIN} element={<LoginPage />} />
        <Route path={HOME} element={<HomePage />} />
      </Routes>
    </BrowserRouter>
  );
};
