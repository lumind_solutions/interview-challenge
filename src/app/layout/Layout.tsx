import React from "react";
import styled from "styled-components";

interface Props {
  children: React.ReactNode;
}

const Root = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const Strip = styled.div`
  height: 10px;
  background-color: ${({ theme }) => theme.colors.primaryColor};
`;

const Container = styled.div`
  max-width: 1024px;
  width: 100%;
  margin: 0 auto;
  padding: 0 1rem;
  flex-grow: 1;
`;

export const Layout = ({ children }: Props) => {
  return (
    <Root>
      <Strip />
      <Container>{children}</Container>
    </Root>
  );
};
