import React from "react";
import styled from "styled-components";
import { Product } from "./Product";
import { FilterItem, FilterList } from "./Filter";

const ProductList = styled.div`
  margin-top: 16px;
  padding: 0;
`;

export const HomePage = () => {
  return (
    <div>
      <FilterList>
        <FilterItem>Technology</FilterItem>
        <FilterItem>Household</FilterItem>
      </FilterList>
      <ProductList>
        <Product
          title={"Great Product"}
          description={"This product will make you feel awesome"}
          imageSrc={"https://picsum.photos/200"}
          category={"Technology"}
        />
        <Product
          title={"Another Product"}
          description={"This product is also a must buy"}
          imageSrc={"https://picsum.photos/220"}
          category={"Household"}
        />
      </ProductList>
    </div>
  );
};
