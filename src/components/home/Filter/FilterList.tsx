import React from "react";
import styled from "styled-components";

export const FilterList = styled.ul`
  padding: 8px 0;
  list-style-type: none;
`;
