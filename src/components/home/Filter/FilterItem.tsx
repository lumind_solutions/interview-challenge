import React from "react";
import styled from "styled-components";
import { lighten } from "polished";

interface Props {
  children: React.ReactText;
  onClick?: () => void;
  className?: string;
}

const Root = styled.li`
  display: inline-block;
  padding: 8px 16px;
  cursor: pointer;
  background-color: ${({ theme }) => lighten(0.2, theme.colors.primaryColor)};
  border-radius: 16px;
  margin-right: 8px;
  font-weight: bold;
  font-size: 0.8rem;
  color: white;
  &:last-child {
    margin-right: 0;
  }
  &:hover {
    background-color: ${({ theme }) => theme.colors.primaryColor};
  }
`;

export const FilterItem = ({ children, onClick, className }: Props) => {
  return (
    <Root className={className} onClick={onClick}>
      {children}
    </Root>
  );
};
