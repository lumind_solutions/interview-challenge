import React from "react";
import styled from "styled-components";
import { Tag } from "./Tag";

interface Props {
  imageSrc: string;
  title: string;
  description: string;
  category: string;
  onClick?: (e: React.MouseEvent<HTMLDivElement>) => void;
}

const Root = styled.div`
  color: ${({ theme }) => theme.colors.darkGrey};
  display: flex;
  height: 100px;
  padding: 8px;
  margin-bottom: 8px;
  cursor: pointer;
  border-radius: 4px;
  &:hover {
    background-color: rgba(255, 255, 255, 0.15);
  }
`;

const Img = styled.img`
  height: 100%;
  width: auto;
  border-radius: 4px;
  margin-right: 12px;
`;

const Column = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.h3`
  margin: 4px 0;
`;

const Description = styled.p`
  margin: 0;
  flex-grow: 1;
`;

const StyledTag = styled(Tag)`
  align-self: flex-start;
`;

export const Product = ({
  title,
  description,
  imageSrc,
  category,
  onClick,
}: Props) => {
  return (
    <Root onClick={onClick}>
      <Img src={imageSrc} />
      <Column>
        <Title>{title}</Title>
        <Description>{description}</Description>
        <StyledTag>{category}</StyledTag>
      </Column>
    </Root>
  );
};
