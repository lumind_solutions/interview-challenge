import React from "react";
import styled from "styled-components";

interface Props {
  className?: string;
  children: React.ReactText;
}

const Root = styled.div`
  display: inline-block;
  background-color: ${({ theme }) => theme.colors.primaryColor};
  color: white;
  border-radius: 2px;
  font-size: 0.6rem;
  padding: 4px 8px;
`;

export const Tag = ({ children, className }: Props) => {
  return <Root className={className}>{children}</Root>;
};
