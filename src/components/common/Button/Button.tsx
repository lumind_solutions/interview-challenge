import React from "react";
import styled from "styled-components";
import {lighten} from "polished";

export const Button = styled.button`
  background-color: ${({theme}) => theme.colors.primaryColor};
  padding: 8px 16px;
  color: white;
  border: none;
  margin-top: 8px;
  cursor: pointer;
  font-family: ${({theme}) => theme.fontFamily};
  &:hover {
    background-color: ${({theme}) => lighten(0.1, theme.colors.primaryColor)};
  }
`;
