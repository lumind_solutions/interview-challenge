import React, { InputHTMLAttributes } from "react";
import styled from "styled-components";
import { lighten } from "polished";

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
}

const Root = styled.div`
  margin-bottom: 8px;
`;

const Label = styled.label`
  display: block;
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.primaryColor};
`;

const StyledInput = styled.input`
  padding: 8px;
  outline: none;
  border: ${({ theme }) => `2px solid ${theme.colors.primaryColor}`};
  border-radius: 2px;
  background: transparent;
  color: ${({ theme }) => theme.colors.darkGrey};
  font-family: ${({ theme }) => theme.fontFamily};
  &:focus {
    border-color: ${({ theme }) => lighten(0.25, theme.colors.primaryColor)};
  }
`;

export const Input = ({ label, ...inputProps }: Props) => {
  return (
    <Root>
      {label && <Label htmlFor={inputProps.id}>{label}</Label>}
      <StyledInput {...inputProps} />
    </Root>
  );
};
