import React from "react";
import styled from "styled-components";

export const Card = styled.div`
  border: ${({ theme }) => `2px solid ${theme.colors.primaryColor}`};
  border-radius: 4px;
  padding: 16px;
`;
