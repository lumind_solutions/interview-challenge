import React from "react";
import styled from "styled-components";
import { Card } from "./Card";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { login } from "./login-api";

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const LoginPage = () => {
  return (
    <Root>
      <Card>
        <Input label={"Username"} />
        <Input type={"password"} label={"Password"} />
        <Button onClick={() => login("admin", "password")}>Login</Button>
      </Card>
    </Root>
  );
};
